# Anja Bettendorf, Nov/Dez 2020
#############################################################
#
# Instructions how to run web application to view and edit the datasets "Countries" and "Earthquakes"
#
# Basics taken from an example tutorial for a "books-store".
# Thanks to Dushan Kumarasinghe for creating the turorial!
#############################################################################

# enter folder with cloned files
cd countries-earthquakes

#create virtual python environment
pyenv versions
pyenv local 3.8.6
python3 -m venv name-environ

# activate virtual python environment
source name-environ/bin/activate

# when finished, deactivate environment
deactivate 

---------------------------------------------

# install Flask
pip install Flask
FLASK_APP=app.py

# create database
sudo -u name_of_user createdb countries-earthquakes
psql -U name_of_user -d countries-earthquakes
# show relations in database:
\dt
# quit database:
\q

# create configurations
export APP_SETTINGS="config.DevelopmentConfig"
export DATABASE_URL="postgresql:///countries-earthquakes"

# install for database migration
pip install flask_sqlalchemy
pip install flask_migrate
pip install psycopg2-binary

# initialize database (has to be run only once)
flask db init
# migrate database
flask db migrate -m "Initial migration"
# update database
flask db upgrade

# run web application with flask
flask run

# click on link "* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
"
--> web application opens in web browser.

#Possible apps:

1)
http://127.0.0.1:5000/getall

Get all entries of the database. Show them in web browser in json-format.

2)
http://127.0.0.1:5000/add-country/form

Add country properties by entering text and values in html-form in web browser. An additional entry in the database is created. This entry can be viewed by refreshing the /getall app.

3)
http://127.0.0.1:5000/json-add

Add a complete geojson or json file to the database. The name of the file to add has to by specified in the file "app.py". After that the "flask run" has to be restarted (stopped and started again).
