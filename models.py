# Anja Bettendorf, Nov/Dez 2020
#############################################################################
#
# Web application to view and edit the datasets "Countries" and "Earthquakes"
#
# Basics taken from an example tutorial for a "books-store".
# Thanks to Dushan Kumarasinghe for creating the turorial!
#############################################################################

from app import db
from sqlalchemy.dialects.postgresql import JSON


class Countries(db.Model):
    __tablename__ = 'countrydata'

    id = db.Column(db.Integer, primary_key=True)
    json_column = db.Column(JSON)

    def __init__(self, json_column):
        self.json_column = json_column

    def serialize(self):
        return {
            'id': self.id, 
            'json_column': self.json_column,
        }
