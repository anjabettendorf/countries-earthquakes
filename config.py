# Anja Bettendorf, Nov/Dez 2020
#############################################################################
#
# Web application to view and edit the datasets "Countries" and "Earthquakes"
#
# Basics taken from an example tutorial for a "books-store".
# Thanks to Dushan Kumarasinghe for creating the turorial!
#############################################################################

import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
