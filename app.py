# Anja Bettendorf, Nov/Dez 2020
#############################################################################
#
# Web application to view and edit the datasets "Countries" and "Earthquakes"
#
# Basics taken from an example tutorial for a "books-store".
# Thanks to Dushan Kumarasinghe for creating the turorial!
#############################################################################

import os
from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
import json

app = Flask(__name__)

# use SQLAlchemy to transfer changes to database
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# import class Countries
from models import Countries

# Hello World
@app.route("/")
def hello():
    return "Hello World!"

# show all entries of the database in web browser
@app.route("/getall")
def get_all():
    try:
        countrydata=Countries.query.all()
        return  jsonify([e.serialize() for e in countrydata])
    except Exception as e:
	    return(str(e))
        
# add a country via a html form 
@app.route("/add-country/form",methods=['GET', 'POST'])
def add_country_form():
    if request.method == 'POST':
        # get inserted text from html form
        abbrev=request.form.get('abbrev')
        continent=request.form.get('continent')
        economy=request.form.get('economy')
        formal_en=request.form.get('formal_en')
        GDP_MD_EST=request.form.get('GDP_MD_EST')
        GDP_year=request.form.get('GDP_year')
        INCOME_GRP=request.form.get('INCOME_GRP')
        Lastcensus=request.form.get('Lastcensus')
        name=request.form.get('name')
        name_long=request.form.get('name_long')
        pop_est=request.form.get('pop_est')
        pop_rank=request.form.get('pop_rank')
        pop_year=request.form.get('pop_year')
        postal=request.form.get('postal')
        region_un=request.form.get('region_un')
        region_wb=request.form.get('region_wb')
        SOV_A3=request.form.get('SOV_A3')
        subregion=request.form.get('subregion')
        try:
            # save in format of class Countries
            countrydata=Countries(
                json_column=[{"ABBREV": abbrev,
                              "CONTINENT": continent,
                              "ECONOMY": economy,
                              "FORMAL_EN": formal_en,
                              "GDP_MD_EST": GDP_MD_EST,
                              "GDP_YEAR": GDP_year,
                              "INCOME_GRP": INCOME_GRP,
                              "LASTCENSUS": Lastcensus,
                              "NAME": name,
                              "NAME_LONG": name_long,
                              "POP_EST": pop_est,
                              "POP_RANK": pop_rank,
                              "POP_YEAR": pop_year,
                              "POSTAL": postal,
                              "REGION_UN": region_un,
                              "REGION_WB": region_wb,
                              "SOV_A3": SOV_A3,
                              "SUBREGION": subregion
                             }]
            )
            # add to database
            db.session.add(countrydata)
            db.session.commit()
            return "Country added. Country id={}".format(countrydata.id)
        except Exception as e:
            return(str(e))
    return render_template("getdata-country.html")

# load .geojson files into database
@app.route("/json-add")
def insert_data():
    # read data from geojson file
    with open('Countries.geojson') as f:
    #with open('Earthquakes.geojson') as f:
        data = json.load(f)

    # use class Countries to convert data to table format 
    countries = Countries(json_column=data)

    # write data into database
    db.session.add(countries)
    db.session.commit()
    return "Data added."

if __name__ == '__main__':
    app.run()

